#include "uSerial.h"
#include <stdexcept>

using namespace std;

static const char *GetWinError( void );

/******************************************************************************/
Serial::Serial( std::string _comport )
    :  m_hComm( INVALID_HANDLE_VALUE ), m_index( 0 )
{
    m_hComm = 0;
    strncpy( m_comport, _comport.c_str(), 256 );
}

/******************************************************************************/
Serial::~Serial()
{
    if( m_hComm )
    {
        close();
    }
}
/******************************************************************************/
void Serial::open( void )
{
    BOOL fSuccess;
    COMMTIMEOUTS CommTimeouts;
    DCB dcb;

    if( m_hComm == INVALID_HANDLE_VALUE )
    {
        return;
    }

    m_hComm = CreateFileA( m_comport,
                           GENERIC_READ | GENERIC_WRITE,
                           0,
                           NULL,
                           OPEN_EXISTING,
                           0,
                           NULL );

    if( m_hComm == INVALID_HANDLE_VALUE )
    {
        throw runtime_error( "CreateFileA: " + string( GetWinError() ) );
    }

    fSuccess = SetupComm( m_hComm, 1024, 1024 );

    if( !fSuccess )
    {
        throw runtime_error( "SetupComm: " + string( GetWinError() ) );
    }


    fSuccess = GetCommState( m_hComm, &dcb );

    if( !fSuccess )
    {
        throw runtime_error( "GetCommState: " + string( GetWinError() ) );
    }

    fSuccess = BuildCommDCBA( "baud=2000000 parity=N data=8 stop=1", &dcb );

    if( !fSuccess )
    {
        throw runtime_error( "SetCommState: " + string( GetWinError() ) );
    }

    fSuccess = SetCommState( m_hComm, &dcb );

    if( !fSuccess )
    {
        throw runtime_error( "SetCommState: " + string( GetWinError() ) );
    }

    CommTimeouts.ReadIntervalTimeout         =   0;
    CommTimeouts.ReadTotalTimeoutMultiplier  =   0;
    CommTimeouts.ReadTotalTimeoutConstant    =   0;
    CommTimeouts.WriteTotalTimeoutMultiplier =   0;
    CommTimeouts.WriteTotalTimeoutConstant   =   0;
    fSuccess = SetCommTimeouts( m_hComm, &CommTimeouts );

    if( !fSuccess )
    {
        throw runtime_error( "SetCommTimeouts: " + string( GetWinError() ) );
    }

    fSuccess = SetCommMask( m_hComm, EV_RXCHAR );

    if( !fSuccess )
    {
        throw runtime_error( "SetCommMask: " + string( GetWinError() ) );
    }

    fSuccess = PurgeComm( m_hComm,
                          PURGE_RXCLEAR |
                          PURGE_TXCLEAR |
                          PURGE_RXABORT |
                          PURGE_TXABORT );

    if( !fSuccess )
    {
        throw runtime_error( "PurgeComm: " + string( GetWinError() ) );
    }
}

/******************************************************************************/
void Serial::close( void )
{
    if( m_hComm != INVALID_HANDLE_VALUE )
    {
        CloseHandle( m_hComm );
    }

    m_hComm = INVALID_HANDLE_VALUE;
}

/******************************************************************************/
void Serial::send( const std::string &cmd )
{
    DWORD cnt;
    BOOL fSuccess;
    unsigned int msglen;
    //char ans[256];

    char cmdcr[256] = "";
    strncpy( cmdcr, cmd.c_str(), 256 );
    msglen = strlen( cmdcr );

    fSuccess = WriteFile( m_hComm, cmdcr, msglen, &cnt, 0 );

    if( !fSuccess || msglen != cnt )
    {
        throw runtime_error( "WriteFile: " + string( GetWinError() ) );
    }
}

/******************************************************************************/
std::string Serial::recv()
{
    DWORD dwEventMask;
    BOOL fSuccess;

    fSuccess = WaitCommEvent( m_hComm, &dwEventMask, NULL );

    if( !fSuccess )
    {
        throw runtime_error( "PurgeComm: " + string( GetWinError() ) );
    }

    char ch;
    DWORD nrread;

    do
    {
        fSuccess = ReadFile( m_hComm,
                             &ch,
                             sizeof( ch ),
                             &nrread,
                             NULL );

        if( !fSuccess )
        {
            throw runtime_error( "ReadFile: " + string( GetWinError() ) );
        }

        m_buffer[m_index] = ch;
        ++m_index;

        if( ch == '\n' )
        {
            m_buffer[m_index - 1] = 0;
            m_index = 0;
            return m_buffer;
        }

        if( m_index >= 255 )
        {
            m_buffer[m_index] = 0;
            m_index = 0;
            return m_buffer;
        }
    }

    while( nrread > 0 );

    return "";
}



/******************************************************************************/
static const char *GetWinError( void )
{
    LPVOID lpMsgBuf;
    static char buf[256];

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        nullptr,
        GetLastError(),
        MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), // Default language
        ( LPTSTR ) &lpMsgBuf,
        0,
        nullptr
    );

    strncpy( buf, ( const char * )lpMsgBuf, 256 );

    LocalFree( lpMsgBuf );

    return buf;
}
