#ifndef RUNNER_H
#define RUNNER_H

#define NRDIGIN (21)
#define NRANAIN (5)

void start_worker( void );

extern bool dig_inputs[NRDIGIN];
extern int ana_inputs[NRANAIN];
extern bool running;
extern bool stopped;

#endif
