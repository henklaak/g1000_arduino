
#if IBM
#include <windows.h>
#endif
#include <string.h>
#include <stdio.h>

#include "XPLMPlugin.h"
#include "XPLMMenus.h"

static void ReloadPluginsMenuHandler(void * mRef, void * iRef);

/******************************************************************************/
PLUGIN_API int XPluginStart(char *outName,
	char *outSig,
	char *outDesc)
{
	strcpy(outName, "LaakSoftReloadPlugin");
	strcpy(outSig, "laaksoft.reload.plugin");
	strcpy(outDesc, "LaakSoftReloadPlugin");

	XPLMMenuID  id;
	int         item;


	item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "LaakSoftReloadPlugin", NULL, 1);

	id = XPLMCreateMenu("LaakSoftReloadPlugin", XPLMFindPluginsMenu(), item, ReloadPluginsMenuHandler, NULL);
	XPLMAppendMenuItem(id, "Reload", (void *)"Reload", 1);

	return 1;
}

/******************************************************************************/
PLUGIN_API void XPluginStop(void)
{
}

/******************************************************************************/
PLUGIN_API int XPluginEnable(void)
{
	return 1;
}

/******************************************************************************/
PLUGIN_API void XPluginDisable(void)
{
}

/******************************************************************************/
PLUGIN_API void XPluginReceiveMessage(
	XPLMPluginID    inFromWho,
	int             inMessage,
	void *          inParam)
{
}


void ReloadPluginsMenuHandler(void * mRef, void * iRef)
{
	if (!strcmp((char *)iRef, "Reload"))
	{
		XPLMReloadPlugins();
	}
}
