#if IBM
#include <windows.h>
#endif
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <math.h>

#include "XPLMPlugin.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"
#include "XPLMUtilities.h"
#include "runner.h"

// From ini file, defaults
int MIXTURE_LEAN = 0;
int MIXTURE_RICH = 1023;


#define AI_FLAPS      (0)
#define AI_IGNITION   (1)
#define AI_MIXTURE    (2)

#define DI_AVIONICS   (8)
#define DI_BATTERY   (10)
#define DI_BEACON     (0)
#define DI_FUELPUMP  (11)
#define DI_GENERATOR  (9)
#define DI_HEATER    (12)
#define DI_LANDING    (1)
#define DI_NAV        (3)
#define DI_PARKING   (20)
#define DI_SPEEDBRK   (7)
#define DI_STROBE     (4)
#define DI_TANKLFT   (17)
#define DI_TANKRGT   (19)
#define DI_TAXI       (2)
#define DI_TRIMDN    (16)
#define DI_TRIMUP    (15)

#define DO_SPEEDBRK   (0)
#define DO_BEACON     (1)

#define POLLPERIOD (0.1)

static XPLMCommandRef refAvionicsOn, refAvionicsOff;
static XPLMCommandRef refBattery1On, refBattery1Off;
static XPLMCommandRef refBattery2On, refBattery2Off;
static XPLMCommandRef refBeaconLightsOn, refBeaconLightsOff;
static XPLMCommandRef refCrossTieOn, refCrossTieOff;
static XPLMCommandRef refFuelPumpOn, refFuelPumpOff;
static XPLMCommandRef refGeneratorOn, refGeneratorOff;
static XPLMCommandRef refLandingLightsOn, refLandingLightsOff;
static XPLMCommandRef refNavLightsOn, refNavLightsOff;
static XPLMCommandRef refParkingBrakeOn, refParkingBrakeOff;
static XPLMCommandRef refPitotHeaterOn, refPitotHeaterOff;
static XPLMCommandRef refSpeedBrkOn, refSpeedBrkOff;
static XPLMCommandRef refStarter;
static XPLMCommandRef refStrobeLightsOn, refStrobeLightsOff;
static XPLMCommandRef refTaxiLightsOn, refTaxiLightsOff;
static XPLMCommandRef refTankNone, refTankLft, refTankRgt, refTankAll;
static XPLMCommandRef refTrimUp, refTrimDn;

static XPLMDataRef refBeaconIsOn;
static XPLMDataRef refFlaps;
static XPLMDataRef refIgnitionKey;
static XPLMDataRef refMixture;
static XPLMDataRef refParkingBrake;
static XPLMDataRef refSpeedBrkIsOn;

static float MyFlightLoopCallback(float, float, int, void*);

using namespace std;

/******************************************************************************/
PLUGIN_API int XPluginStart(char *outName,
    char *outSig,
    char *outDesc)
{
    strcpy(outName, "LaakSoftTestPlugin");
    strcpy(outSig, "laaksoft.test.plugin");
    strcpy(outDesc, "LaakSoftTestPlugin");

    //TODO ini file
    MIXTURE_LEAN = 466;
    MIXTURE_RICH = 811;
    SERIAL_PORT = "\\\\.\\COM6";

    refAvionicsOn       = XPLMFindCommand("sim/systems/avionics_on");
    refAvionicsOff      = XPLMFindCommand("sim/systems/avionics_off");
    refBattery1On       = XPLMFindCommand("sim/electrical/battery_1_on");
    refBattery1Off      = XPLMFindCommand("sim/electrical/battery_1_off");
    refBattery2On       = XPLMFindCommand("sim/electrical/battery_2_on");
    refBattery2Off      = XPLMFindCommand("sim/electrical/battery_2_off");
    refBeaconLightsOn   = XPLMFindCommand("sim/lights/beacon_lights_on");
    refBeaconLightsOff  = XPLMFindCommand("sim/lights/beacon_lights_off");
    refCrossTieOn       = XPLMFindCommand("sim/electrical/cross_tie_on");
    refCrossTieOff      = XPLMFindCommand("sim/electrical/cross_tie_off");
    refFuelPumpOn       = XPLMFindCommand("sim/fuel/fuel_pumps_on");
    refFuelPumpOff      = XPLMFindCommand("sim/fuel/fuel_pumps_off");
    refGeneratorOn      = XPLMFindCommand("sim/electrical/generator_1_on");
    refGeneratorOff     = XPLMFindCommand("sim/electrical/generator_1_off");
    refLandingLightsOn  = XPLMFindCommand("sim/lights/landing_lights_on");
    refLandingLightsOff = XPLMFindCommand("sim/lights/landing_lights_off");
    refNavLightsOn      = XPLMFindCommand("sim/lights/nav_lights_on");
    refNavLightsOff     = XPLMFindCommand("sim/lights/nav_lights_off");
    refPitotHeaterOn    = XPLMFindCommand("sim/ice/pitot_heat0_on");
    refPitotHeaterOff   = XPLMFindCommand("sim/ice/pitot_heat0_off");
    refSpeedBrkOn       = XPLMFindCommand("sim/flight_controls/speed_brakes_down_all");
    refSpeedBrkOff      = XPLMFindCommand("sim/flight_controls/speed_brakes_up_all");
    refStarter          = XPLMFindCommand("sim/engines/engage_starters");
    refStrobeLightsOn   = XPLMFindCommand("sim/lights/strobe_lights_on");
    refStrobeLightsOff  = XPLMFindCommand("sim/lights/strobe_lights_off");
    refTankAll          = XPLMFindCommand("sim/fuel/fuel_selector_all");
    refTankLft          = XPLMFindCommand("sim/fuel/fuel_selector_lft");
    refTankNone         = XPLMFindCommand("sim/fuel/fuel_selector_none");
    refTankRgt          = XPLMFindCommand("sim/fuel/fuel_selector_rgt");
    refTaxiLightsOn     = XPLMFindCommand("sim/lights/taxi_lights_on");
    refTaxiLightsOff    = XPLMFindCommand("sim/lights/taxi_lights_off");
    refTrimDn           = XPLMFindCommand("sim/flight_controls/pitch_trim_down");
    refTrimUp           = XPLMFindCommand("sim/flight_controls/pitch_trim_up");

    refBeaconIsOn       = XPLMFindDataRef("sim/cockpit/electrical/beacon_lights_on");
    refFlaps            = XPLMFindDataRef("sim/flightmodel/controls/flaprqst");
    refIgnitionKey      = XPLMFindDataRef("sim/cockpit2/engine/actuators/ignition_key");
    refMixture          = XPLMFindDataRef("sim/cockpit2/engine/actuators/mixture_ratio_all");
    refParkingBrake     = XPLMFindDataRef("sim/cockpit2/controls/parking_brake_ratio");
    refSpeedBrkIsOn     = XPLMFindDataRef("sim/cockpit/warnings/annunciators/speedbrake");

    XPLMRegisterFlightLoopCallback(MyFlightLoopCallback, POLLPERIOD, NULL);

    return 1;
}

/******************************************************************************/
PLUGIN_API void XPluginStop(void)
{
}

/******************************************************************************/
PLUGIN_API int XPluginEnable(void)
{
#ifdef WIN32
    try
    {
        start_worker();
        while (!running)
            Sleep(10);
    }
    catch( const std::exception &e )
    {
        cout << "Plugin failed: " << e.what() << endl;
        XPLMDebugString("LaakSoftTestPlugin Enable failed");
        return 0;
    }
#endif
    XPLMDebugString("LaakSoftTestPlugin enabled");
    return 1;
}

/******************************************************************************/
PLUGIN_API void XPluginDisable(void)
{
#ifdef WIN32
    running = false;
    while (!stopped)
        Sleep(10);
#endif
    XPLMDebugString("LaakSoftTestPlugin disabled");
}

/******************************************************************************/
PLUGIN_API void XPluginReceiveMessage(
    XPLMPluginID    inFromWho,
    int             inMessage,
    void *          inParam)
{
}

/******************************************************************************/
/*
0 - Bovenste rij 1, 0 = aan
1 - Bovenste rij 2, 0 = aan
2 - Bovenste rij 3, 0 = aan
3 - Bovenste rij 4, 0 = aan
4 - Bovenste rij 5, 0 = aan
5 - Bovenste rij 6, 0 = aan

6 - nc?
7 - nc?
8 - master, 0 = aan
9 - gen, 0 = aan
0 - bat, 0 = aan

1 - Onderste rij 1, 0 = aan
2 - Onderste rij 1, 0 = aan
3 - Onderste rij 3, 0 = aan
4 - Onderste rij 4, 0 = aan

5 - pitch up, 0 = aan
6 - pitch dn, 0 = aan
7 - fuel tank r, 0 = aan
8 - nc?
9 - fuel tank r, 0 = aan
0 - break, 0 = aan

0 - flaps 1024 = up, 512 = mid, 0 = down
1 - ignition, 0 = off, 256 = 1, 512 = 2, 768 = both, 1024 = starter
2 - mixture, 466 = lean, 811 = rich
3 - nc?
4 - nc?

-------------------------------------------
22 - Beacon lights switch
23 - Landing lights Switch (all).
24 - Taxi lights Switch
25 - Nav lights Switch.
26 - Strobe lights switch
27 - Spot lights Switch

28 - static air
30 - Speebrake On/Off / Speebrake On/Off
31 - Avionics Power On (Master switch)
32 - Generator 1 On/Off
33 - Battery 1 Switch / Battery 2 Switch

34 - Fuel pumps switch – all engines
35 - Left Pitot Heat switch / Right Pitot Heat switch
36 - Master anti-ice system Switch (All)
37 - Panel Flood light Switch

38 - Pitch trim up (+) / Pitch trim up (+)
39 - Pitch trim down (-) / Pitch trim down (-)
40 - fuel tank
41 - Right brake Off/Full (switch button) / Left brake Off/Full (switch button)
42 - Fuel Tank
43 - Parking brake None/Full (simple switch)*/

float MyFlightLoopCallback(float inElapsedSinceLastCall,
    float inElapsedTimeSinceLastFlightLoop,
    int   inCounter,
    void *inRefcon)
{
#ifndef WIN32
    static float total_time = 0;
    total_time += inElapsedTimeSinceLastFlightLoop;

    double A;
    int dA;
    A = 0.5 + 0.5 * sin(total_time * 1/10000.0);
    dA = int(A+0.5);

    ana_inputs[AI_FLAPS    ] = 0;
    ana_inputs[AI_IGNITION ] = 768;
    ana_inputs[AI_MIXTURE  ] = MIXTURE_RICH;

    dig_inputs[DI_AVIONICS ] = 0;
    dig_inputs[DI_BATTERY  ] = 0;
    dig_inputs[DI_BEACON   ] = 0;
    dig_inputs[DI_FUELPUMP ] = 0;
    dig_inputs[DI_GENERATOR] = 0;
    dig_inputs[DI_HEATER   ] = 0;
    dig_inputs[DI_LANDING  ] = 0;
    dig_inputs[DI_NAV      ] = 0;
    dig_inputs[DI_PARKING  ] = 1;
    dig_inputs[DI_SPEEDBRK ] = 0;
    dig_inputs[DI_STROBE   ] = 0;
    dig_inputs[DI_TANKLFT  ] = 0;
    dig_inputs[DI_TANKRGT  ] = 0;
    dig_inputs[DI_TAXI     ] = 0;
    dig_inputs[DI_TRIMDN   ] = 1;
    dig_inputs[DI_TRIMUP   ] = 1;
#endif
    {
        static int old_flaps = -1;
        int flaps = int((1023.0 - ana_inputs[AI_FLAPS]) / 512.0 + 0.5);
        if (old_flaps != flaps)
        {
            old_flaps = flaps;
            XPLMSetDataf(refFlaps, flaps*0.5);
        }
    }
    {
        static int old_ignition = -1;
        static bool starting = false;
        int ignition = int(ana_inputs[AI_IGNITION] / 256.0 + 0.5);
        if (old_ignition != ignition)
        {
            old_ignition = ignition;

            if (ignition == 4)
            {
                if (!starting)
                {
                    starting = true;
                    XPLMCommandBegin(refStarter);
                }
            }
            else
            {
                if (starting)
                {
                    starting = false;
                    XPLMCommandEnd(refStarter);
                }
                XPLMSetDatavi(refIgnitionKey, &ignition, 0, 1);
            }
        }
    }
    {
        double mixture = (double(ana_inputs[AI_MIXTURE]) - MIXTURE_LEAN) /
                             (MIXTURE_RICH - MIXTURE_LEAN);
        mixture = max(0.0, min(1.0, mixture));

        XPLMSetDataf(refMixture, mixture);
    }
    {
        static int old_avionics_on = -1;
        int avionics_on = dig_inputs[DI_AVIONICS] ? 0 : 1;
        if (old_avionics_on != avionics_on)
        {
            old_avionics_on = avionics_on;
            XPLMCommandOnce(avionics_on ? refAvionicsOn : refAvionicsOff);
            XPLMCommandOnce(avionics_on ? refCrossTieOn : refCrossTieOff);
        }
    }
    {
        static int old_battery_on = -1;
        int battery_on = dig_inputs[DI_BATTERY] ? 0 : 1;
        if (old_battery_on != battery_on)
        {
            old_battery_on = battery_on;
            XPLMCommandOnce(battery_on ? refBattery1On : refBattery1Off);
            XPLMCommandOnce(refBattery2Off);
        }
    }
    {
        static int old_beacon_lights_on = -1;
        int beacon_lights_on = dig_inputs[DI_BEACON] ? 0 : 1;
        if (old_beacon_lights_on != beacon_lights_on)
        {
            old_beacon_lights_on = beacon_lights_on;
            XPLMCommandOnce(beacon_lights_on ? refBeaconLightsOn : refBeaconLightsOff);
        }
    }
    {
        static int old_fuel_pump_on = -1;
        int fuel_pump_on = dig_inputs[DI_FUELPUMP] ? 0 : 1;
        if (old_fuel_pump_on != fuel_pump_on)
        {
            old_fuel_pump_on = fuel_pump_on;
            XPLMCommandOnce(fuel_pump_on ? refFuelPumpOn : refFuelPumpOff);
        }
    }
    {
        static int old_generator_on = -1;
        int generator_on = dig_inputs[DI_GENERATOR] ? 0 : 1;
        if (old_generator_on != generator_on)
        {
            old_generator_on = generator_on;
            XPLMCommandOnce(generator_on ? refGeneratorOn : refGeneratorOff);
        }
    }
    {
        static int old_pitot_heater_on = -1;
        int pitot_heater_on = dig_inputs[DI_HEATER] ? 0 : 1;
        if (old_pitot_heater_on != pitot_heater_on)
        {
            old_pitot_heater_on = pitot_heater_on;
            XPLMCommandOnce(pitot_heater_on ? refPitotHeaterOn : refPitotHeaterOff);
        }
    }
    {
        static int old_landing_lights_on = -1;
        int landing_lights_on = dig_inputs[DI_LANDING] ? 0 : 1;
        if (old_landing_lights_on != landing_lights_on)
        {
            old_landing_lights_on = landing_lights_on;
            XPLMCommandOnce(landing_lights_on ? refLandingLightsOn : refLandingLightsOff);
        }
    }
    {
        static int old_nav_lights_on = -1;
        int nav_lights_on = dig_inputs[DI_NAV] ? 0 : 1;
        if (old_nav_lights_on != nav_lights_on)
        {
            old_nav_lights_on = nav_lights_on;
            XPLMCommandOnce(nav_lights_on ? refNavLightsOn : refNavLightsOff);
        }
    }
    {
        float parking_brake_on = dig_inputs[DI_PARKING] ? 0.0 : 1.0;
        XPLMSetDataf(refParkingBrake, parking_brake_on);
    }
    {
        static int old_speedbrk_on = -1;
        int speedbrk_on = dig_inputs[DI_SPEEDBRK] ? 0 : 1;
        if (old_speedbrk_on != speedbrk_on)
        {
            old_speedbrk_on = speedbrk_on;
            XPLMCommandOnce(speedbrk_on ? refSpeedBrkOn : refSpeedBrkOff);
        }
    }
    {
        static int old_strobe_lights_on = -1;
        int strobe_lights_on = dig_inputs[DI_STROBE] ? 0 : 1;
        if (old_strobe_lights_on != strobe_lights_on)
        {
            old_strobe_lights_on = strobe_lights_on;
            XPLMCommandOnce(strobe_lights_on ? refStrobeLightsOn : refStrobeLightsOff);
        }
    }
    {
        static int old_tank_lft = -1;
        static int old_tank_rgt = -1;

        int tank_lft = dig_inputs[DI_TANKLFT] ? 0 : 1;
        int tank_rgt = dig_inputs[DI_TANKRGT] ? 0 : 1;

        if ((old_tank_lft != tank_lft) || (old_tank_rgt != tank_rgt))
        {
            old_tank_lft = tank_lft;
            old_tank_rgt = tank_rgt;

            if ( tank_lft &&  tank_rgt) XPLMCommandOnce(refTankNone);
            if ( tank_lft && !tank_rgt) XPLMCommandOnce(refTankRgt);
            if (!tank_lft &&  tank_rgt) XPLMCommandOnce(refTankLft);
            if (!tank_lft && !tank_rgt) XPLMCommandOnce(refTankAll);
        }
    }
    {
        static int old_taxi_lights_on = -1;
        int taxi_lights_on = dig_inputs[DI_TAXI] ? 0 : 1;
        if (old_taxi_lights_on != taxi_lights_on)
        {
            old_taxi_lights_on = taxi_lights_on;
            XPLMCommandOnce(taxi_lights_on ? refTaxiLightsOn : refTaxiLightsOff);
        }
    }
    {
        static int old_trim_dn = -1;
        int trim_dn = dig_inputs[DI_TRIMDN] ? 0 : 1;
        if (old_trim_dn != trim_dn)
        {
            old_trim_dn = trim_dn;
            if (trim_dn) XPLMCommandBegin(refTrimDn);
            else         XPLMCommandEnd(refTrimDn);
        }
    }
    {
        static int old_trim_up = -1;
        int trim_up = dig_inputs[DI_TRIMUP] ? 0 : 1;
        if (old_trim_up != trim_up)
        {
            old_trim_up = trim_up;
            if (trim_up) XPLMCommandBegin(refTrimUp);
            else         XPLMCommandEnd(refTrimUp);
        }
    }
    {
        int speedbrkison = XPLMGetDatai(refSpeedBrkIsOn);
        dig_outputs[DO_SPEEDBRK] = speedbrkison;
    }
    {
        int beaconison = XPLMGetDatai(refBeaconIsOn);
        dig_outputs[DO_BEACON] = beaconison;
    }

    return POLLPERIOD;
}

