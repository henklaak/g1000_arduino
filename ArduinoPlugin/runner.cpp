#include "runner.h"
#ifdef WIN32
#include <windows.h>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <sstream>
#include "uSerial.h"
#endif

using namespace std;

string SERIAL_PORT = "COM1";

bool dig_inputs[NRDIGIN];
bool dig_outputs[NRDIGOUT];
int ana_inputs[NRANAIN];

bool running = false;
bool stopped = true;

static int old_dig_outputs[NRDIGOUT] = { -1,-1 };


#ifdef WIN32

std::vector<std::string> split( const std::string &s, char delimiter )
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream( s );

    while( std::getline( tokenStream, token, delimiter ) )
    {
        tokens.push_back( token );
    }

    return tokens;
}

/******************************************************************************/
DWORD WINAPI  worker( LPVOID lpParam )
{
    try
    {
        running = true;
        stopped = false;
        Serial ser( SERIAL_PORT );

        ser.open();

        while( running )
        {
            string ans = ser.recv();

            if( ans[0] == '=' )
            {
                string base = ans.substr( 1 );

                vector<string> elems = split( base, ',' );

                // digital inputs
                for( int i = 0; i < NRDIGIN; ++i )
                {
                    dig_inputs[i] = ( base[i] == '1' );
                }

                for( int i = 0; i < NRANAIN; ++i )
                {
                    int val = stoi( elems[1 + i] );
                    ana_inputs[i] = val;
                }
            }

            bool output_needed = false;

            for (int i=0; i<NRDIGOUT; ++i)
            {
                if (old_dig_outputs[i] != int(dig_outputs[i]))
                {
                    old_dig_outputs[i] = int(dig_outputs[i]);
                    output_needed = true;
                }
            }

            if (output_needed)
            {
                char buf[16];
                snprintf(buf, 16, "%d%d\n", int(dig_outputs[0]), int(dig_outputs[1]));
                ser.send(buf);
            }
        }

        ser.close();
    }
    catch( const std::exception &e )
    {
        running = false;
        cout << "Thread exception: " << e.what() << endl;
    }

    stopped = true;
    return 0;
}

/******************************************************************************/
void start_worker( void )
{
    DWORD threadid = 0;
    HANDLE h;

    h = CreateThread( NULL, 0, worker, 0, 0, &threadid );

    if( h == NULL )
    {
        throw runtime_error( "CreateThread" );
    }
}
#endif
