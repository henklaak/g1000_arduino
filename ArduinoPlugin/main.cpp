#include <windows.h>
#include <iostream>
#include "runner.h"

using namespace std;

int main( int argc, char *argv[] )
{
    cout << "started" << endl;

    try
    {
        start_worker();

        for( int i = 0; i < 1000; i++ )
        {
            cout << ana_inputs[0] << " " << running << endl;
            Sleep( 100 );

            if( !running )
            {
                throw  runtime_error( "Thread died" );
            }
        }

        running = false;
    }
    catch( const std::exception &e )
    {
        cout << "Exception: " << e.what() << endl;
    }

    return 0;
}
